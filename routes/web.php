<?php

use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'CardController@index') -> name('home');
Route::get('/card','CardController@showCard') -> name('showCard');
Route::get('/admin/login', 'PageController@adminInput') -> name('adminInput');
Route::post('/admin/login', 'PageController@login') -> name('adminLogin');
Route::get('/admin/panel', 'AdminController@showPanel') -> name('adminPanel');
Route::get('/admin/logout', 'AdminController@logout') -> name('logout');
Route::get('/admin/add-category', 'AdminController@addCategory') -> name('add-category');
Route::post('/admin/add-category', 'AdminController@storeCategory') -> name('store-category');
Route::get('/admin/edit-category', 'AdminController@editCategory') -> name('edit-category');
Route::post('/admin/edit-category', 'AdminController@updateCategory') -> name('update-category');
Route::get('/admin/add-lecture', 'AdminController@addLecture') -> name('add-lecture');
Route::post('/admin/add-lecture', 'AdminController@storeLecture') -> name('store-lecture');
Route::get('/admin/delete-lecture', 'AdminController@deleteLecture') -> name('delete-lecture');
Route::get('/admin/add-banner', 'AdminController@addBanner') -> name('add-banner');
Route::post('/admin/add-banner', 'AdminController@storeBanner') -> name('store-banner');
Route::get('/admin/edit-banner', 'AdminController@editBanner') -> name('edit-banner');
Route::post('/admin/edit-banner', 'AdminController@updateBanner') -> name('update-banner');
Route::get('/admin/show-subscribers', 'AdminController@showSubscribers') -> name('show-subscribers');
Route::post('/subscribe', 'SubscriberController@subscribe') -> name('subscribe');