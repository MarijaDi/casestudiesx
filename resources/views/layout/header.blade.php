<nav class="navbar  navbarStyle">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed sandwitchButton" data-toggle="collapse"
                data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="icon-bar sandwitchLine"></span>
                <span class="icon-bar sandwitchLine"></span>
                <span class="icon-bar sandwitchLine"></span>
            </button>
            <a class="navbar-brand marginLogo" href="{{route('home')}}"><img
                    src="{{ asset('images/brainster-logo.png') }}" alt="" class="navbarLogoPhoto img-center"></a>
        </div>
            <div class="collapse navbar-collapse navbarLinks" id="bs-example-navbar-collapse-1">
                @if(session()->has('admin'))
                <a class="btn buttonNavigation admin" href="{{route('adminPanel')}}" role="button">Admin</a>

                @endif
                <a class="btn buttonNavigation" href="{{url('https://www.brainster.co/courses?type=all')}}"
                    target="_blank" role="button">Академии</a>
                <a class="btn buttonNavigation" href="{{url('https://brainster.co/courses?type=deepdive')}}"
                    target="_blank" role="button">Семинари</a>
                <a class="btn buttonNavigation" href="{{url('https://brainster.co/')}}" target="_blank"
                    role="button">Тест за кариера</a>
                <a class="btn buttonNavigation" href="{{url('​https://blog.brainster.co/')}}" target="_blank"
                    role="button">Блог</a>
                <a class="btn buttonJoin" role="button" data-toggle="modal"
                    {{session()->has('admin') ? 'data-target="" disabled' : 'data-target=#subscribe-category'}}>Пријави
                    се</a>
            </div>
    </div>
</nav>

<div class="modal fade" id="subscribe-category" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modalDialog" role="document">
        <div class="modal-content modalContent">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Пријави се</h4>
            </div>
            <div class="modal-body">
                <form action="{{route('subscribe')}}" method="post" class="modalForm">
                    @csrf

                    <label for="subscriber_email" class="subscribeFormLabel">Емаил</label>
                    <input type="email" name="subscriber_email" id="subscriber_email" class="subscriberFormInput">


                    @if(Route::is('home'))
                    <label for="category" class="subscribeFormLabel">Одбери категорија</label>
                    <select name="category" id="category" class="subscriberFormInput subscriberFormSelect">
                        <option value="">Категорија</option>
                        @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                    </select>
                    @endif
                    @if(Route::is('showCard'))
                    <input type="text" name="category" id="category" value="{{$category->id}}" hidden>
                    @endif
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Затвори</button>
                <button type="submit" class="btn btn-primary">Пријави се</button>
            </div>
            </form>
        </div>
    </div>
</div>