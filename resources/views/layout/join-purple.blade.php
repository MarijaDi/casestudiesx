<div class="row text-center subscribeRow">
    <div class="col-md-12 subscribeColumn">
        <p class="subscribe-main-text subscribe-first-p">Приклучи се на {{$enthusiasts}} ентузијасти и учи дигитални вештини.</p>
        <p class="subscribe-main-text">Бесплатно.</p>
        <form action="{{route('subscribe')}}" method="post" >
            @csrf

            <input type="email" name="subscriber_email" placeholder="Email Address" class="emailInput">

            @if(Route::is('showCard'))
            <input type="text" name="category" id="category" value="{{$category->id}}" hidden>
            @else
            <input type="text" name="category" id="category" hidden>
            @endif

            <button type="submit" class="subscribeButton" 
                    {{session()->has('admin') ? 'disabled' : ''}}>Пријави се</button>
        </form>
        <p class="subscribe-bottom-text">Можеш да се исклучиш од листата на маилови во секое време!</p>
    </div>
</div>