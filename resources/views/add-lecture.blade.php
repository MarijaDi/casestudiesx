@extends('layout.main')

@section('main-content')

<div class="container">

    <div class="row">
        <div class="col-md-10 col-md-offset-2 col-sm-10 col-sm-offset-2 col-xs-12 addLectureColumn">
            <p class="formIntro">Додај лекција <span class="requiredForm">*сите полиња се задолжителни</span></p>
            <form action="{{route('store-lecture')}}" method="post" class="addLectureForm"
                enctype="multipart/form-data">
                @csrf


                <label for="category" class="lectureFormLabel">Селектирај категорија @error('category')
                    <span class="errorFormStar">*</span>
                    @enderror</label>
                <select name="category" id="category">
                    <option value="">Категорија</option>
                    @foreach($categories as $category)
                    <option value="{{$category->id}}" @if(old('category') == $category->id) selected @endif >{{$category->name}}</option>
                    @endforeach
                </select>

                <label for="title" class="lectureFormLabel">Наслов <span class="lengthInput"><span id="titleCount"></span>/150</span></label>
                <input type="text" name="title" id="title" class="lectureFormInput" maxlength="150" value="{{old('title')}}">
                @error('title')
                <span class="errorForm">Името треба да има помалку од 150 карактери</span>
                @enderror

                <label for="description" class="lectureFormLabel">Опис <span class="lengthInput"><span id="descriptionCount">0</span>/65500</span></label>
                <textarea name="description" id="description" cols="5" rows="5" class="textAreaForm">{{old('description')}}</textarea>
                @error('description')
                <span class="errorForm">Описот треба да има помалку од 65500 карактери</span>
                @enderror

                <label for="upload_date" class="lectureFormLabel">Датум на прикачување @error('upload_date')
                    <span class="errorFormStar">*</span>
                    @enderror
                </label>
                <input type="date" name="upload_date" id="upload_date" value="{{old('upload_date')}}">


                <button type="submit" class="submitForm">Додај</button>
            </form>
        </div>
    </div>
</div>


@endsection