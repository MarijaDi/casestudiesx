@extends('layout.main')

@section('main-content')

<div class="container">

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <table class="table table-striped">
                <tr>
                    <th>#</th>
                    <th>Емаил</th>
                    <th>Категорија</th>
                    <th>Датум на регистрирање</th>
                </tr>
                @foreach($subscribers as $subscriber)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$subscriber->email}}</td>
                    @if($subscriber->category)
                    <td>{{$subscriber->category->name}}</td>
                    @else
                    <td>/</td>
                    @endif
                    <td>{{ date('M jS, Y', strtotime($subscriber->created_at))}}</td>
                </tr>
                @endforeach
            </table>
        </div>

    </div>

</div>


@endsection