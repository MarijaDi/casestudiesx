@extends('layout.main')

@section('main-content')


<div class="container">

    @include('layout.join-purple')

    <div class="row cardsSection">
        <div class="col-md-10 col-md-offset-1">
            @foreach($categories as $category)
            @if($loop->first || ($loop->iteration % 3) == 1) <div class="row"> @endif
                <div class="col-md-4 col-sm-4 col-xs-12 card">
                    <a href='{{route("showCard", ["id" => $category->id])}}' target="_blank">
                        <div class="row">
                            <div class="col-md-12 card-min-height">
                                <p class="text-center">
                                    <img src='{{ asset("images/categories-photo/$category->photo") }}' alt=""
                                        class="categoryPhoto">
                                </p>
                                <p class="category-title">{{$category->name}}</p>
                                <p class="category-desc">{{$category->description}}</p>
                                <p class="lectures-number">{{count($category->lectures)}} лекции</p>
                            </div>
                        </div>
                    </a>
                    @if(session()->has('admin'))
                    <div class="row">
                        <div class="col-md-12">
                            <a href="{{route('edit-category', ['id' => $category->id])}}">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                    @endif
                </div>
                @if(($loop->iteration % 3) == 0 || $loop->last)
            </div>
            @endif
            @endforeach
        </div>
    </div>
</div>
@endsection