@extends('layout.main')

@section('main-content')

<div class="container">

    @include('layout.join-purple')

    <div class="row">
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-12 lectures">
                    @foreach($lectures as $lecture)
                    <div class="row lectureRow">
                        <div class="col-md-12 lectureColumn">
                            <p class="lecture-title"><span
                                    class="lecture-number">#{{$loop->iteration}}</span>{{$lecture->title}}<span
                                    class="lecture-date">{{ date('M jS Y', strtotime($lecture->upload_date))}}</span>
                            </p>
                            <p class="text-fit">{{$lecture->description}}

                                @if(session()->has('admin'))
                                <a class="deleteLecture btn" data-toggle="modal"
                                    data-target="#deleteLecture">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </a>
                                @endif
                            </p>
                        </div>
                    </div>
                    <div class="modal fade" id="deleteLecture" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog modalDialog" role="document">
                            <div class="modal-content modalContent">
                                <div class="modal-body">
                                   Дали сте сигурни дека сакате да ја избришете лекцијата?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Не</button>
                                    <a href="{{route('delete-lecture', ['id' => $lecture->id])}}" class="btn btn-primary">Да</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>

        </div>
        <div class="col-md-3 bannerColumn">

            @if($banners)
            @foreach($banners as $banner)
            <p class="banner-title">{{$banner->title}}</p>
            <p class="banner-text text-fit">{{$banner->description}}</p>
            <p class="banner-link-p">
                <a href="{{$banner->link}}" class="banner-link" target="_blank">Повеќе <span class="banner-arrow"><i
                            class="fa fa-arrow-right" aria-hidden="true"></i></span></a>
            </p>
            <p class="edit-icon">
                @if(session()->has('admin'))
                <a href="{{route('edit-banner', ['id' => $banner->id])}}">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                </a>
                @endif
            </p>
            @endforeach
            @endif

        </div>
    </div>
</div>

@endsection