@extends('layout.main')

@section('main-content')

<div class="container">

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <form action="{{route('adminLogin')}}" method="POST" class="loginForm">
                @csrf
                <label for="email" class="labelLogin">Емаил</label>
                <input type="email" name="email" id="email" class="inputLogin" required>
                <label for="password" class="labelLogin">Лозинка</label>
                <input type="password" name="password" id="password" class="inputLogin" required>
                <button type="submit" class="buttonLogin">Логирај се</button>
            </form>
        </div>
    </div>
</div>

@endsection