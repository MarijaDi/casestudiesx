@extends('layout.main')

@section('main-content')

<div class="container">

    <div class="row">
        <div class="col-md-12 adminColumn">
            <a href="{{route('add-category')}}" class="adminLinks">Додај категорија</a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 adminColumn"> <a href="{{route('add-lecture')}}" class="adminLinks">Додај лекција</a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 adminColumn"> <a href="{{route('add-banner')}}" class="adminLinks">Додај банер</a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 adminColumn">
            <a href="{{route('show-subscribers')}}" class="adminLinks">Листа на регистрации</a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 adminColumn">
            <a href="{{route('logout')}}" class="adminLinks">Одлогирај се</a>
        </div>
    </div>

</div>

@endsection