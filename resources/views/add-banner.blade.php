@extends('layout.main')

@section('main-content')

<div class="container">

    <div class="row">
        <div class="col-md-10 col-md-offset-2 col-sm-10 col-sm-offset-2 col-xs-12 addLectureColumn">
            <p class="formIntro">Додај банер <span class="requiredForm">*сите полиња се задолжителни</span></p>
            <form @if(Route::is('edit-banner')) action={{route('update-banner', ["id" => $banner->id])}} @else
                action={{route('store-banner')}} @endif method="post" class="addLectureForm"
                enctype="multipart/form-data">
                @csrf

                @if(Route::is('edit-banner'))
                <label for="category" class="lectureFormLabel">Kатегорија</label>
                <input type="text" placeholder="{{$banner->category->name}}" disabled>
                @else

                <label for="category" class="lectureFormLabel">Селектирај категорија @error('category')
                    <span class="errorFormStar">*</span>
                    @enderror</label>
                <select name="category" id="category">
                    <option value="">Категорија</option>
                    @foreach($categories as $category)
                    <option value="{{$category->id}}" @if(old('category')==$category->id) selected @endif
                        >{{$category->name}}</option>
                    @endforeach
                </select>

                @endif

                <label for="title" class="lectureFormLabel">Наслов <span class="lengthInput"><span id="titleCount">0</span>/150</span></label>
                <input type="text" name="title" id="title" class="lectureFormInput" maxlength="150"
                    @if(Route::is('edit-banner')) value="{{$banner->title}}" @else value="{{old('title')}}" @endif>
                @error('title')
                <span class="errorForm">Насловот треба да има помалку од 150 карактери</span>
                @enderror

                <label for="description" class="lectureFormLabel">Опис  <span class="lengthInput"><span id="descriptionCount">0</span>/255</span></label>
                <textarea name="description" id="description" cols="5" rows="5"
                    class="textAreaForm">@if(Route::is('edit-banner')){{$banner->description}}@else{{old('description')}}@endif</textarea>
                @error('description')
                <span class="errorForm">Описот треба да има помалку од 255 карактери</span>
                @enderror
                <label for="link" class="lectureFormLabel">Линк</label>
                <input type="url" name="link" id="link"
                    @if(Route::is('edit-banner')){{$banner->link}}@else{{old('link')}}@endif>
                @error('link')
                <span class="errorFormStar">Линкот не е валиден</span>
                @enderror


                <button type="submit" class="submitForm">Додај</button>
            </form>
        </div>
    </div>
</div>


@endsection