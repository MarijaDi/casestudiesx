@extends('layout.main')

@section('main-content')

<div class="container">

    <div class="row">
        <div class="col-md-10 col-md-offset-2 col-sm-10 col-sm-offset-2 col-xs-12 addCategoryColumn">
            <p class="formIntro">Додај категорија <span class="requiredForm">*сите полиња се задолжителни</span></p>
            <form @if(Route::is('edit-category')) action="{{route('update-category', ['id' => $category->id])}}" @else
                action="{{route('store-category')}}" @endif method="post" class="addCategoryForm"
                enctype="multipart/form-data">
                @csrf

                <label for="name" class="categoryFormLabel">Име на категорија <span class="lengthInput"><span id="nameCount">0</span>/15</span></label>
                <input type="text" name="name" id="name" class="categoryFormInput" maxlength="15" 
                    @if(Route::is('edit-category')) value="{{$category->name}}" disabled @else
                    value="{{old('name')}}" @endif>
                @error('name')
                <span class="errorForm">Името треба да има помалку од 15 карактери</span>
                @enderror

                <label for="photo" class="categoryFormLabel">Слика</label>
                <input type="file" name="photo" id="photo" class="categoryFormFile">
                @error('photo')
                <span class="errorForm">Сликата треба да е помалку од 5 мегабајти</span>
                @enderror

                <label for="description" class="categoryFormLabel">Опис <span class="lengthInput"><span id="descriptionCount">0</span>/400</span></label>
                <textarea name="description" id="description" cols="5" rows="5" class="textAreaForm" maxlength="400"
                    >{{Route::is('edit-category') ? "$category->description" : old("description")}}</textarea>
                @error('description')
                <span class="errorForm">Описот треба да има помалку од 400 карактери</span>
                @enderror

                <button type="submit" class="submitForm">{{Route::is('edit-category') ? 'Промени' : 'Додај'}}
                </button>
            </form>
        </div>
    </div>
</div>


@endsection