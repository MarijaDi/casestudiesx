let description = document.getElementById('description')
if (description != null) {
    let descCount = document.getElementById('descriptionCount')
    let value = description.value
    descCount.innerText = value.length
    description.addEventListener('keyup', function(e) {
        let descCount = document.getElementById('descriptionCount')
        let value = description.value
        descCount.innerText = value.length
    })
}


let title = document.getElementById('title')
if (title != null) {
    let titleCount = document.getElementById('titleCount')
    let value = title.value
    titleCount.innerText = value.length
    title.addEventListener('keyup', function(e) {
        let titleCount = document.getElementById('titleCount')
        let value = title.value
        titleCount.innerText = value.length
    })
}


let name = document.getElementById('name')
if (name != null) {
    let nameCount = document.getElementById('nameCount')
    let value = name.value
    nameCount.innerText = value.length
    name.addEventListener('keyup', function(e) {
        let nameCount = document.getElementById('nameCount')
        let value = name.value
        nameCount.innerText = value.length
    })
}