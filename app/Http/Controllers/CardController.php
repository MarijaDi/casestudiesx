<?php

namespace App\Http\Controllers;

use App\Category;
use App\Subscriber;
use DateTime;
use Illuminate\Http\Request;

class CardController extends Controller
{
    public function index(){

        $categories = Category::all();
        $enthusiasts = Subscriber::all();
        $num = $enthusiasts->count();
        
        $data = [
            'categories' => $categories,
            'enthusiasts' => $num,
        ];
        
        return view('landing-page')->with($data);

    }

    public function showCard(Request $request){

        $category = Category::find($request->id);
        $enthusiasts = Subscriber::all();

        $lectures = $category->lectures;
        $banners = $category->banners;
        $num = $enthusiasts->count();

        $data = [
            'category' => $category,
            'lectures' => $lectures,
            'enthusiasts' => $num,
            'banners' => $banners,
        ];
        
        return view('card-page')->with($data);
    }
}