<?php

namespace App\Http\Controllers;

use App\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class PageController extends Controller
{
    public function adminInput(){
        return view('login');
    }

    public function login(Request $request){

        $admin = Admin::where('email', $request->email)->first();

        if(!$admin || !Hash::check($request->password, $admin->password)){

            $request->session()->flash('message', 'Incorrect credentials');
            $request->session()->flash('message_type', 'danger');
            return redirect()->back();
        }

            $request->session()->put('admin', $admin->email);
            $request->session()->flash('message_type', 'success');
            $request->session()->flash('message', 'Admin is logged in');
        return redirect()->route('home');

    }
}