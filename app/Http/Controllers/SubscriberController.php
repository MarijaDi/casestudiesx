<?php

namespace App\Http\Controllers;

use App\Subscriber;
use Illuminate\Http\Request;

class SubscriberController extends Controller
{
    public function subscribe(Request $request){

        $validation = $request->validate([
            'subscriber_email' => 'required|unique:subscribers,email|email',
            'category' => 'nullable|exists:categories,id',
        ]);

        $subscriber = new Subscriber();
        $subscriber->email = $request->subscriber_email;
        if($request->category)
            $subscriber->category_id = $request->category;
        $subscriber->save();


        $request->session()->flash('message_type', 'success');
        $request->session()->flash('message', 'You have successfully subscribed');
        return redirect()->back();
    }
}
