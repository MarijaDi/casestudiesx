<?php

namespace App\Http\Controllers;

use App\Banner;
use App\Category;
use App\Lecture;
use App\Subscriber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{
    public function __construct(){
        $this->middleware('admin');
    }
    
    public function showPanel(){
        return view('admin-panel');
    }

    public function logout(){
        if(session()->has('admin')){
            session()->forget('admin');
            session()->flash('message_type', 'warning');
            session()->flash('message', 'Admin is logged off');           
        }
        
        return redirect()->route('home');
    }

    public function addCategory(){
        return view('add-category');
    }

    public function storeCategory(Request $request){
        
        $validation = $request->validate([
            'name' => 'required|max:15',
            'photo' => 'required|max:5120',
            'description' => 'required|max:400',
        ]);

        $path =Storage::disk('public')->put('categories-photo', $request->photo);
        $photo = basename($path);



        $category = new Category();
        $category->name = $request->name;
        $category->photo = $photo;
        $category->description = $request->description;
        $category->save();

        
        $request->session()->flash('message_type', 'success');
        $request->session()->flash('message', 'Category added.');
        return redirect()->back();
    }


    public function editCategory(Request $request){
        $category = Category::find($request->id);

        return view('add-category')->with('category', $category);        
    }

    public function updateCategory(Request $request){
        
        $category = Category::find($request->id);

        if(!$category){
            $request->session()->flash('message_type', 'danger');
            $request->session()->flash('message', 'Category not found.');
            return redirect()->back();
        }

        $validation = $request->validate([
            'description' => 'required|max:400',
        ]);

        $category->description = $request->description;

        if($request->photo){
            $validation = $request->validate([
                'photo' => 'required|max:5120',
        ]);

        $path =Storage::disk('public')->put('categories-photo', $request->photo);
        $photo = basename($path);
        $category->photo = $photo;
        
        }

        $category->save();

        $request->session()->flash('message_type', 'success');
        $request->session()->flash('message', 'Category updated.');
        return redirect()->route('adminPanel');
    }

    public function addLecture(){

        $categories = Category::all();

        return view('add-lecture')->with('categories', $categories);
    }

    public function storeLecture(Request $request){

        $validation = $request->validate([
            'category' => 'required|exists:categories,id',
            'title' => 'required|max:150',
            'description' => 'required|max:65500',
            'upload_date' => 'required',
        ]);

        $lecture = new Lecture();

        $lecture->category_id = $request->category;
        $lecture->title = $request->title;
        $lecture->description = $request->description;
        $lecture->upload_date = $request->upload_date;
        $lecture->save();

        $request->session()->flash('message_type', 'success');
        $request->session()->flash('message', 'Lecture added.');
        return redirect()->back();
    }

    public function deleteLecture(Request $request){

        $lecture = Lecture::find($request->id);        
        $lecture->delete();

        $request->session()->flash('message_type', 'info');
        $request->session()->flash('message', 'Lecture deleted.');
        return redirect()->back();
    }
    
    public function addBanner(){

        $categories = Category::all();

        return view('add-banner')->with('categories', $categories);
    }

    public function storeBanner(Request $request){

        $validation = $request->validate([
            'category' => 'required|exists:categories,id',
            'title' => 'required|max:150',
            'description' => 'required|max:255',
            'link' => 'required|url',
        ]);

        $banner = new Banner();

        $banner->category_id = $request->category;
        $banner->title = $request->title;
        $banner->description = $request->description;
        $banner->link = $request->link;
        $banner->save();

        $request->session()->flash('message_type', 'success');
        $request->session()->flash('message', 'Banner added.');
        return redirect()->back();
    }


    public function editBanner(Request $request){
        $banner = Banner::find($request->id);

        $banner->load('category');

       return view('add-banner')->with('banner', $banner);
    }

    public function updateBanner(Request $request){
        $banner = Banner::find($request->id);

        if(!$banner){
        $request->session()->flash('message_type', 'danger');
        $request->session()->flash('message', 'Banner not found.');
        return redirect()->back();
        }



        $validation = $request->validate([
            'title' => 'required|max:150',
            'description' => 'required|max:255',
            'link' => 'required|url',
        ]);

        $banner->title = $request->title;
        $banner->description = $request->description;
        $banner->link = $request->link;
        $banner->save();


        $request->session()->flash('message_type', 'success');
        $request->session()->flash('message', 'Banner updated.');
        return redirect()->route('adminPanel');
    }


    public function showSubscribers(){

        $subscribers = Subscriber::all();
        $subscribers->load('category');

        return view('subscribers')->with('subscribers', $subscribers);
    }
    
}