<?php

namespace App\Http\Middleware;

use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(session()->has('admin')){
            return $next($request);
        }
        
        session()->flash('message_type', 'danger');
        session()->flash('message', 'Insufficient permission');
        return redirect()->route('home');
    }
}