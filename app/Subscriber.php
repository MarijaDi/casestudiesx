<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    public function category(){
        return $this->belongsTo('App\Category');
    }
}
