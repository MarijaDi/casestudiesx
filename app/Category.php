<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function lectures(){
        return $this->hasMany('App\Lecture');
    }

    public function banners(){
        return $this->hasMany('App\Banner');
    }
}
